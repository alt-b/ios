# iOS
### 目次
* [ユーザーインターフェイスガイドラインまとめ](https://gitlab.com/alt-b/ios/-/blob/master/guideline.md)  
* [端末種類](https://gitlab.com/alt-b/ios/-/blob/master/device.md)  
* [アイコン](https://gitlab.com/alt-b/ios/-/blob/master/icon.md)  
* [スプラッシュ画面](https://gitlab.com/alt-b/ios/-/blob/master/splash.md)  
* [困った時の手引き](https://gitlab.com/alt-b/ios/-/blob/master/guidance.md)  
* [最新情報](https://)  
